#!/bin/sh

# Designed for RHEL8 derivatives.
# Tested on a Rocky Linux 8.5 Workstation installation.
# Author: Michael Holley
# URL: https://gitlab.com/mholley01/discord-update-utility


discord_location="https://discord.com/api/download?platform=linux&format=tar.gz"

force_install=0
tempdir=$(mktemp -d)
discord_destination="discord.tar.gz"
local_bin="${HOME}/.local/bin"
script_dir="$(cd $(dirname $0) && pwd 2>/dev/null)"
skip_launch=0
distribution='rhel'

cleanup () {
    printf "Cleaning up temporary directory...\n"
    if [ -d "${tempdir}" ]; then
        rm -rf "${tempdir}" 2>/dev/null
    fi
}
trap cleanup EXIT;

while getopts 'fx' option; do
    case "${option}" in
      f) force_install=1;;
      x) skip_launch=1;;
      ?) printf 'Unrecognized flag\n';;
    esac
done

cd "${script_dir}"

# Operating System variant detection.
for a in $(sed -n -E -e 's/^ID_LIKE="?([^"]*)"?$/\1/p' /etc/os-release); do
    case $a in
        rhel)
            printf "RHEL variant detected.\n"
            distribution='rhel'
        ;;
        debian)
            printf "Debian variant detected.\n"
            distribution='debian'
        ;;
    esac;
done

. "./${distribution}_specifics.sh"

retrieve_package () {
    printf "Downloading Discord package...\n"
    download_application "${1}" "${discord_location}"
}

update_discord () {
    printf "Updating Discord...\n"
    discord_binary="${local_bin}/Discord/Discord"
    mkdir -p "${local_bin}" 2>&1 >/dev/null
    mv -t "${local_bin}" "${tempdir}/${discord_destination}"
    _old_pwd="$(pwd)"
    cd "${local_bin}"
    printf "Killing any currently running Discord process...\n"
    for pid in $(ps -aux \
        | grep -E "${discord_binary}" \
        | grep -v 'grep' \
        | tr -s ' ' \
        | cut -d ' ' -f2)
    do
        kill -9 ${pid}
    done
    printf "Unpacking tarball...\n"
    tar -zpxf "${discord_destination}"
    cd "${_old_pwd}"
    rm "${discord_destination}" 2>/dev/null
    mv -t . "${local_bin}/${discord_destination}"
    printf "Installing 'discord' command...\n"
    if [ ! -f "${local_bin}/discord" ]; then
        printf "#!/bin/bash\n"'"'"${local_bin}/Discord/Discord"'" & disown\n' > "${local_bin}/discord"
        chmod +x "${local_bin}/discord"
    fi
    if [ "${skip_launch}" -eq 0 ]; then
        printf "Launching Discord...\n"
        "${local_bin}/discord"
    fi
    printf 'Update complete.\n'
}

if [ ! -f "${discord_destination}" ] || [ "${force_install}" -eq 1 ]; then
    retrieve_package "${tempdir}/${discord_destination}"
    update_discord
else
    local_checksum="$(md5sum "${discord_destination}" | cut -d ' ' -f1)"
    retrieve_package "${tempdir}/${discord_destination}"
    printf "Comparing local and newly obtained checksums...\n"
    remote_checksum="$(md5sum "${tempdir}/${discord_destination}" | cut -d ' ' -f1)"
    if [ "${local_checksum}" != "${remote_checksum}" ]; then
        update_discord
     else
        printf "Checksums match. No update will be performed.\n"
    fi
fi
