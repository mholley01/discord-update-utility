### README

The Discord Update Utility (`discord-updater.sh`) script was initially written to be invoked using bash on a RHEL8 derivative. It will download the most recent tarball package from Discord's servers (The first variable assignment in the script is the location of the tarball, it may need to be updated from time to time.). Once the remote tarball is downloaded the script will check for any local tarball with the file name 'discord.tar.gz' within its directory, and compare md5 checksums between the local and remote files. If there is a difference in the checksums or if the force install flag is provided (**-f**), the script will

1. Kill any Discord instance running from `${HOME}/.local/bin`
2. Unpack remote tarball to `${HOME}/.local/bin`
3. Remove the outdated local tarball.
4. Move the remote tarball to the location of the script.
5. Create a convenience `discord` script in `${HOME}/.local/bin` to easily invoke Discord from your terminal, if it does not already exist.
6. Execute discord (invoking the update script with **-x** disables this step).
7. Clean up temporary directory used for remote tarball download.

**This script was mainly created to ease the frustration involved in both installing and keeping Discord up to date on my personal workstation, and as such bugs may be present.**

### Examples

#### Force installation of Discord from remote tarball
```sh
./discord-updater.sh -f
```

#### Regular installation, but avoid launching Discord.
```sh
./discord-updater.sh -x
```

#### Force installation, and do not launch Discord.
```sh
./discord-updater.sh -fx
```

#### Regular update (check for newer remote tarball, and launch Discord).
```sh
./discord-updater.sh
```
