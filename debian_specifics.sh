# This file is meant to house debian specific functionality

download_application () {
    wget --progress=bar -O "${1}" "${2}"
}
